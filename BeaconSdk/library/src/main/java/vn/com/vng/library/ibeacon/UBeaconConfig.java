package vn.com.vng.library.ibeacon;

/**
 * Created by AnhHieu on 6/8/15.
 */
public class UBeaconConfig {

    private long appId;
    private String beaconLayout;
    private IBeaconListener listener;
    private long intervalTime;

    private boolean debugLog;

    public boolean isDebugLog() {
        return debugLog;
    }

    public void setDebugLog(boolean debugLog) {
        this.debugLog = debugLog;
    }

    private UBeaconConfig() {
        intervalTime = 5 * 60 * 1000;
        appId = -1;
    }

    public long getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(long intervalTime) {
        this.intervalTime = intervalTime;
    }

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }

    public String getBeaconLayout() {
        return beaconLayout;
    }

    public void setBeaconLayout(String beaconLayout) {
        this.beaconLayout = beaconLayout;
    }

    public IBeaconListener getListener() {
        return listener;
    }

    public void setListener(IBeaconListener listener) {
        this.listener = listener;
    }

    public static class Builder {

        private UBeaconConfig config;

        public Builder() {
            config = new UBeaconConfig();
        }

        public Builder appId(long appId) {
            config.appId = appId;
            return this;
        }

        public Builder beaconLayout(String beaconLayout) {
            config.beaconLayout = beaconLayout;
            return this;
        }

        public Builder listener(IBeaconListener listener) {
            config.listener = listener;
            return this;
        }

        public Builder interval(long time) {
            config.intervalTime = time;
            return this;
        }

        public Builder debugLog(boolean values) {
            config.debugLog = values;
            return this;
        }

        public UBeaconConfig build() {
            return config;
        }

    }
}
