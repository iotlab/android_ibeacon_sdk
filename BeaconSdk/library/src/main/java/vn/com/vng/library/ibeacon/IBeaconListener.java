package vn.com.vng.library.ibeacon;

import java.util.List;

import vn.com.vng.library.ibeacon.model.AdsData;
import vn.com.vng.library.ibeacon.model.UBeaconData;


/**
 * Created by AnhHieu on 6/8/15.
 */
public interface IBeaconListener {
    public void onAdsChanged(AdsData data);
    public void onNearBeacons(List<UBeaconData> data);
}
