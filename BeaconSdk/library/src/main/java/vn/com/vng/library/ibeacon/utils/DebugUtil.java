package vn.com.vng.library.ibeacon.utils;

import android.util.Log;

/**
 * Created by AnhHieu on 5/28/15.
 */
public class DebugUtil {
    public static boolean isDebug = true;

    public static void d(String TAG, String mess) {
        if (isDebug)
            Log.d(TAG, mess);
    }
    public static void e(String TAG, String mess) {
        if (isDebug)
            Log.e(TAG, mess);
    }
}
