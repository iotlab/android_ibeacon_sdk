package vn.com.vng.library.ibeacon.request;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.util.Log;


import vn.com.vng.library.ibeacon.UBeacon;
import vn.com.vng.multithreadactions.Action;
import vn.com.vng.multithreadactions.ActionTask;
import vn.com.vng.multithreadactions.Options;


abstract class Request extends Action {

    private static final String TAG = "Request";

    public abstract boolean checkDataAvailablity();//return false for default

    public abstract String getUrl();

    public abstract void buildParams();//do nothing for default

    public abstract Object parseResponse(String response) throws Exception;

    public abstract boolean validateData(Object data);//return true for default

    public abstract void saveData(Object data);//do nothing for default


    protected String mRespone;
    protected Context mContext;
    protected int mRetryCount;
    protected int mConnectionTimeout;
    protected int mReadTimeout;
    protected int mTotalTimeout;
    protected RequestWatcher mWatcher;

    protected Map<String, Object> mParams;


    public Request(Context context, RequestWatcher watcher) {
        super();
        this.mContext = context;
        mRetryCount = 1;
        mConnectionTimeout = 15000;// (3 seconds)
        mReadTimeout = 10000;
        mTotalTimeout = 30000;
        mWatcher = watcher;
        if (mWatcher != null) {
            mWatcher.setActionTask(mActionTask);
        }

        mParams = new Hashtable<String, Object>();

    }

    public Request(Context context) {
        this(context, null);
    }

    public int getRetryCount() {
        return mRetryCount;
    }

    public Request setRetryCount(int mRetryCount) {
        this.mRetryCount = mRetryCount;
        return this;
    }

    public int getConnectionTimeout() {
        return mConnectionTimeout;
    }

    public Request setConnectionTimeout(int mConnectionTimeout) {
        this.mConnectionTimeout = mConnectionTimeout;
        return this;
    }

    public int getReadTimeout() {
        return mReadTimeout;
    }

    public Request setReadTimeout(int mReadTimeout) {
        this.mReadTimeout = mReadTimeout;
        return this;
    }

    public int getTotalTimeout() {
        return mTotalTimeout;
    }

    public Request setTotalTimeout(int mTotalTimeout) {
        this.mTotalTimeout = mTotalTimeout;
        return this;
    }

    @Override
    public void setActionTask(ActionTask actionTask) {
        // TODO Auto-generated method stub
        super.setActionTask(actionTask);
        if (mWatcher != null) {
            mWatcher.setActionTask(mActionTask);
        }
    }

    public Map<String, Object> addParam(String key, Object value) {
        mParams.put(key, value);
        return mParams;
    }

    public Map<String, Object> addParams(Map<String, Object> params) {
        mParams.putAll(params);
        return mParams;
    }

    public Map<String, Object> removeParam(String key) {
        mParams.remove(key);
        return mParams;
    }

    private String buildParamsString() {
        buildParams();
        if (mParams.size() == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        sb.append("?");
        boolean first = true;
        for (Entry<String, Object> entry : mParams.entrySet()) {
            if (first) {
                sb.append(URLEncoder.encode(entry.getKey()))
                        .append("=")
                        .append(URLEncoder.encode(entry.getValue().toString()));
                first = false;
            } else {
                sb.append("&")
                        .append(URLEncoder.encode(entry.getKey()))
                        .append("=")
                        .append(URLEncoder.encode(entry.getValue().toString()));
            }
        }
        return sb.toString();
    }

    public String getRequestString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getUrl())
                .append(buildParamsString());
        return sb.toString();
    }

    private interface Constants {
        boolean ISDEBUG = false;
    }

    protected int doRequest() throws Exception {
        HttpURLConnection conn = null;
        while (true) {
            try {
                URL url = new URL(getRequestString());
                if (Constants.ISDEBUG) {
                    Log.i(TAG, "Open request" + getRequestString());
                }
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setConnectTimeout(mConnectionTimeout);
                conn.setReadTimeout(mReadTimeout);
//		    	new Thread(new InterruptThread(Thread.currentThread(), conn, mTotalTimeout)).start();

                conn.connect();

                int length = conn.getContentLength();
                if (Constants.ISDEBUG) {
                    Log.i(TAG, "length:" + length);
                }
                InputStream is = conn.getInputStream();// url.openStream();
//		    	if (length > 0){
//		    		mRespone = readRespone(is, length);
//		    	} else {
                mRespone = readRespone(is);
//		    	}
                if (Constants.ISDEBUG) {
                    Log.i(TAG, mRespone);
                }
                is.close();
                break;
            } catch (IOException e) {
                if (Constants.ISDEBUG) {
                    Log.e(TAG, "aborted parsing due to I/O: " + e.getMessage());
                    e.printStackTrace();
                }
                throw new Exception("REQUEST_TIMEOUT");
            } catch (Exception e) {
                //	    	e.printStackTrace();
                mRetryCount--;
                if (Constants.ISDEBUG) {
                    Log.e(TAG, e.getMessage());
                }
                if (mRetryCount <= 0) {
                    throw e;
                }
                if (Constants.ISDEBUG) {
                    Log.i(TAG, "Retrying...");
                }
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        return 0;
    }

    private String readRespone(InputStream is) {
        String fullResponse = "";

        BufferedInputStream input = new BufferedInputStream(is);
        int bufferSize = 1024;

        ByteArrayBuffer bufferContainer = new ByteArrayBuffer(bufferSize + 1);

        byte buffer[] = new byte[bufferSize];
        int total = 0;
        int count;
        try {
            while ((count = input.read(buffer)) != -1) {
                //read data to somewhere
                bufferContainer.append(buffer, 0, count);
                total += count;
                // publishing the progress....
                if (mWatcher != null) {
                    mWatcher.notifyReadData(total, false);
                }
            }

            fullResponse = new String(
                    //remove newString or this will cause OutOfmem
                    bufferContainer.buffer()
            );
        } catch (IOException e) {
            if (Constants.ISDEBUG) {
                e.printStackTrace();
            }
        } finally {
            try {
                input.close();
            } catch (IOException e) {
                if (Constants.ISDEBUG) {
                    e.printStackTrace();
                }
            }
        }
        return fullResponse;
    }


    public String getRespone() {
        return mRespone;
    }

    @Override
    public boolean onPreExecute() throws Exception {
        // TODO Auto-generated method stub
        long start = System.currentTimeMillis();
        boolean isDateAvailable = checkDataAvailablity();
        long end = System.currentTimeMillis();
        if (Constants.ISDEBUG) {
            Log.i(TAG, "PreExcuted in " + (end - start) + " ms");
        }
        return (!isDateAvailable);
    }

    @Override
    public boolean onExecute() throws Exception {
        // TODO Auto-generated method stub
        long start = System.currentTimeMillis();
        doRequest();
        long end = System.currentTimeMillis();
        if (Constants.ISDEBUG) {
            Log.i(TAG, "Excuted in " + (end - start) + " ms");
        }
        return true;
    }

    @Override
    public boolean onPostExecute() throws Exception {
        // TODO Auto-generated method stub
        UBeacon.isMainThread();
        long start = System.currentTimeMillis();
        Object data = parseResponse(getRespone());
        if (mActionTask != null) {
            mActionTask.setData(data);
        }

        if (validateData(data)) {
            saveData(data);
        } else {
            throw new Exception("Invalidated");
        }
        long end = System.currentTimeMillis();
        if (Constants.ISDEBUG) {
            Log.i(TAG, "PostExcuted in " + (end - start) + " ms");
        }
        return false;
    }

    @Override
    public String getTaskQueue() {
        // TODO Auto-generated method stub
        return Options.NETWORK_ACTIONS;
    }

    public static class RequestWatcher {
        protected int mAmountStepSize;
        protected int mCurrentAmount;
        protected ActionTask mActionTask;

        public RequestWatcher(int percentageStep) {
            super();
            this.mAmountStepSize = percentageStep;
            this.mCurrentAmount = 0;
        }

        public RequestWatcher() {
            super();
            this.mAmountStepSize = 0;
            this.mCurrentAmount = 0;
        }

        public void notifyReadData(int amount, boolean isPercentage) {
            if (isPercentage) {
                if (amount >= 100) {
                    mCurrentAmount = amount;
                    onRequestAt(mCurrentAmount, isPercentage);
                    return;
                }
                int step = amount - mCurrentAmount;
                if (step > mAmountStepSize) {
                    mCurrentAmount = amount;
                    onRequestAt(mCurrentAmount, isPercentage);
                    return;
                }
            } else {
                int step = amount - mCurrentAmount;
                if (step > mAmountStepSize) {
                    mCurrentAmount = amount;
                    onRequestAt(mCurrentAmount, isPercentage);
                    return;
                }
            }
        }

        public void setActionTask(ActionTask actionTask) {
            mActionTask = actionTask;
        }

        public void onRequestAt(int amount, boolean isPercentage) {
            if (mActionTask != null) {
                if (isPercentage) {
                    List<Integer> states;
                    if (mActionTask.getExcutingMsg() == null) {
                        states = new LinkedList<Integer>();
                    } else if (mActionTask.getExcutingMsg() instanceof List) {
                        states = (List<Integer>) mActionTask.getExcutingMsg();
                    } else {
                        states = new LinkedList<Integer>();
                    }
                    states.add(amount);
                    mActionTask.setExcutingMsg(states);
                } else {
//					mActionTask.setExcutingMsg("Amout " + amount);
                }
                mActionTask.handleState(STATE_EXECUTE_MSG);
            }
        }
    }

}
