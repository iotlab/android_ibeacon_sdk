package vn.com.vng.library.ibeacon;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;


import vn.com.vng.library.ibeacon.utils.DebugUtil;
import vn.com.vng.multithreadactions.MultiThreadActionsManager;
import vn.com.vng.multithreadactions.Options;

import static vn.com.vng.library.ibeacon.utils.AndroidUtil.checkBlueToothAvailability;

/**
 * Created by AnhHieu on 6/1/15.
 */
public class UBeacon {
    public static final String TAG = "UBeaconManager";
    private static Context context;
    private static boolean init = false;

    public static boolean isMainThread(String TAG) {
        boolean ret = Looper.getMainLooper().getThread() == Thread.currentThread();
        DebugUtil.d(TAG, " main thread : " + ret);
        return ret;
    }

    public static boolean isMainThread() {
        boolean ret = Looper.getMainLooper().getThread() == Thread.currentThread();
        DebugUtil.d(TAG, " main thread : " + ret);
        return ret;
    }

    private static void init(Context ctx) {
        if (ctx == null)
            throw new NullPointerException("Context not null");

        if (init) return;
        context = ctx;
        MultiThreadActionsManager
                .initialize((new Options()).buildDefault());
        applicationHandler = new Handler(context.getMainLooper());
        init = true;
    }

    private static volatile Handler applicationHandler = null;
    private final static Integer lock = 1;

    public static void runOnUiThread(Runnable runnable) {
        synchronized (lock) {
            applicationHandler.post(runnable);
        }
    }

    public static Context getContext() {
        return context;
    }

    public static UBeaconInterface start(Context context, UBeaconConfig config) {
        init(context);
        DebugUtil.isDebug = config.isDebugLog();
        final UBeaconClient client = new UBeaconClient(config);
        client.start();


        UBeaconInterface ret = new UBeaconInterface() {
            @Override
            public void lock() {
                client.stop();
            }

            @Override
            public void unlock() {
                client.start();
            }
        };
        return ret;
    }

    private UBeacon() {
    }

}
