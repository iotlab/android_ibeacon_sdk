package vn.com.vng.beaconsdk;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import vn.com.vng.library.ibeacon.UBeacon;
import vn.com.vng.library.ibeacon.request.AbstractRequestEx;

/**
 * Created by AnhHieu on 6/12/15.
 */
public class GetListAdsRequest extends AbstractRequestEx{
    private String url;
    public  GetListAdsRequest(String url){
        this.url=url;
    }

    @Override
    public Object parseData(String data) throws Exception {
        UBeacon.isMainThread(TAG);
        JSONArray dataResp=new JSONArray(data);
        List<AdsInfoData> ret= new ArrayList<AdsInfoData>();
        for (int i=0;i<dataResp.length();++i){
            AdsInfoData item=new AdsInfoData(dataResp.optJSONObject(i));
            ret.add(item);

        }
        return ret;
    }

    @Override
    public void buildParams() {

    }

    @Override
    public String getUrl() {
        return  url;
    }
}
