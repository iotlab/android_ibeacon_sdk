package vn.com.vng.beaconsdk;

import org.json.JSONObject;

import vn.com.vng.library.ibeacon.model.AdsData;

/**
 * Created by AnhHieu on 6/11/15.
 */
public class AdsInfoData {
    public long advertisementId;
    public String title;
    public String content;
    public String startTime;
    public String endTime;
    public String adsImageUrl;
    private int type = TYPE_ITEM;

    public AdsInfoData(JSONObject value) {
        parseJson(value);
    }
    public AdsInfoData(String section,int type) {
        this.title=section;
        this.type=type;
    }

    public AdsInfoData parseJson(JSONObject value) {
        advertisementId = value.optLong("advertisementId");
        title = value.optString("title");
        content = value.optString("content");
        startTime = value.optString("startTime");
        endTime = value.optString("endTime");
        adsImageUrl = value.optString("adsImageUrl");
        return this;
    }


    public static final int TYPE_SECTION = 0;
    public static final int TYPE_ITEM = 1;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
