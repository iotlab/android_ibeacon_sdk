package vn.com.vng.beaconsdk;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.w3c.dom.Text;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import vn.com.vng.library.ibeacon.UBeacon;
import vn.com.vng.library.ibeacon.model.AdsData;
import vn.com.vng.multithreadactions.ActionCallback;

/**
 * Created by AnhHieu on 6/11/15.
 */
public class AdsFragment extends Fragment {

    public static final String TAG = AdsFragment.class.getSimpleName();

    public static Fragment newInstance() {
        return new AdsFragment();
    }

    AdsAdapter adapter;
    ListView list_item;
    TextView text1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new AdsAdapter(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ads_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list_item = (ListView) view.findViewById(R.id.list_item);
        text1 = (TextView) view.findViewById(R.id.text1);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        list_item.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((UApplication) getActivity().getApplicationContext()).setAdsFragment(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((UApplication) getActivity().getApplicationContext()).setAdsFragment(null);
        if (request != null)
            request.cancel();
    }

    private GetListAdsRequest request;

    public void onRefresh(AdsData data) {
        if (TextUtils.isEmpty(data.getToGetAdsUrl()))
            return;
        request = new GetListAdsRequest(data.getToGetAdsUrl());
        request.start(new AdsCallBack());
    }


//    private void test() {
//        List<AdsInfoData> ret = new ArrayList<AdsInfoData>();
//        for (int i = 0; i < 20; i++) {
//            AdsInfoData item = new AdsInfoData();
//            if (i % 3 == 0) {
//                item.setType(AdsInfoData.TYPE_SECTION);
//            }
//            ret.add(item);
//        }
//        updateAds(ret);
//    }

    private void updateAds(List<AdsInfoData> values) {
        adapter.addAll(values);
        text1.setVisibility(adapter.getCount() == 0 ? View.VISIBLE : View.GONE);
    }

    private void updateAdsWithSection(List<AdsInfoData> values) {
        AdsInfoData section = new AdsInfoData(getSection(), AdsInfoData.TYPE_SECTION);
        values.add(section);

        for (int j = 0; j < values.size(); j++) {
            adapter.insert(values.get(j), 0);
        }
        text1.setVisibility(adapter.getCount() == 0 ? View.VISIBLE : View.GONE);
    }

    private String getSection() {
        String dateString = DateFormat.format("HH:mm MM/dd/yyyy", new Date(System.currentTimeMillis())).toString();
        return "Cập nhật lúc " + dateString;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        list_item.setAdapter(null);
    }

    private class AdsCallBack extends ActionCallback {

        public AdsCallBack() {
            super(null, 0);
        }

        @Override
        public void onActionCompleted(Object o) {
            if (getActivity() == null || getActivity().isFinishing())
                return;
            UBeacon.isMainThread(TAG);
            List<AdsInfoData> data = (List<AdsInfoData>) o;
            updateAdsWithSection(data);
        }

        @Override
        public void onActionFailed(Exception error) {
            if (getActivity() == null || getActivity().isFinishing())
                return;
            final String message = error.getMessage();
            if (!TextUtils.isEmpty(message)) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

        @Override
        public void onActionSkipped() {
            super.onActionSkipped();
        }
    }

    private static class AdsAdapter extends ArrayAdapter<AdsInfoData> {
        LayoutInflater inflater;
        ImageLoader imageLoader;

        public AdsAdapter(Context context) {
            super(context, R.layout.row_item_layout);
            inflater = LayoutInflater.from(context);
            imageLoader = VolleySingleton.getInstance().getImageLoader();
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position).getType();
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemViewHolder itemHolder;
            SectionViewHolder sectionHolder;

            if (AdsInfoData.TYPE_ITEM == getItemViewType(position)) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.row_item_layout, parent, false);
                    itemHolder = new ItemViewHolder();
                    itemHolder.text1 = (TextView) convertView.findViewById(R.id.text1);
                    itemHolder.text2 = (TextView) convertView.findViewById(R.id.text2);
                    itemHolder.image = (NetworkImageView) convertView.findViewById(R.id.image);
                    itemHolder.remain_time = (TextView) convertView.findViewById(R.id.remain_time);
                    convertView.setTag(itemHolder);
                } else {
                    itemHolder = (ItemViewHolder) convertView.getTag();
                }
                renderItem(itemHolder, getItem(position), convertView);
            } else {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.row_section_layout, parent, false);
                    sectionHolder = new SectionViewHolder();
                    sectionHolder.text1 = (TextView) convertView.findViewById(R.id.text1);
                    convertView.setTag(sectionHolder);
                } else {
                    sectionHolder = (SectionViewHolder) convertView.getTag();
                }
                renderSection(sectionHolder, getItem(position), convertView);
            }

            return convertView;
        }

        private void renderItem(ItemViewHolder holder, AdsInfoData data, View convertView) {
            holder.text1.setText(data.title);
            holder.text2.setText(data.content);
            holder.image.setImageUrl(data.adsImageUrl, imageLoader);
            holder.remain_time.setText("Từ " + data.startTime + " đến " + data.endTime);
        }

        private void renderSection(SectionViewHolder holder, AdsInfoData data, View convertView) {
            holder.text1.setText(data.title);
        }

        private class ItemViewHolder {
            TextView text1, text2, remain_time;
            NetworkImageView image;
        }

        private class SectionViewHolder {
            TextView text1;
        }
    }


}
