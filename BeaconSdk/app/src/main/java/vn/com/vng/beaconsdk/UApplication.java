package vn.com.vng.beaconsdk;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import vn.com.vng.library.ibeacon.IBeaconListener;
import vn.com.vng.library.ibeacon.UBeaconConfig;
import vn.com.vng.library.ibeacon.UBeaconInterface;
import vn.com.vng.library.ibeacon.UBeacon;
import vn.com.vng.library.ibeacon.model.AdsData;
import vn.com.vng.library.ibeacon.model.UBeaconData;
import vn.com.vng.library.ibeacon.utils.AndroidUtil;

/**
 * Created by AnhHieu on 6/11/15.
 */
public class UApplication extends Application {

    protected UBeaconInterface uBeaconInterface;


    @Override
    public void onCreate() {
        super.onCreate();
        appContect = getApplicationContext();
        initBeaconSdk();

    }

    private static Context appContect;

    public static Context getAppContext() {
        return appContect;
    }

    public void lock() {
        if (uBeaconInterface != null)
            uBeaconInterface.lock();
    }

    public void unlock() {
        if (uBeaconInterface != null)
            uBeaconInterface.unlock();
    }


    private void initBeaconSdk() {
        try {
            AndroidUtil.checkBlueToothAvailability(getApplicationContext());
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),"Exception",Toast.LENGTH_SHORT).show();
            return;
        }
        UBeaconConfig.Builder builder = new UBeaconConfig.Builder()
                .appId(9L)
                .interval(1 * 60 * 1000)
                .beaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")
                .debugLog(true)
                .listener(new IBeaconListener() {
                    @Override
                    public void onAdsChanged(AdsData data) {
                        if (mAdsFragment != null)
                            mAdsFragment.onRefresh(data);
                    }

                    @Override
                    public void onNearBeacons(List<UBeaconData> data) {

                    }
                });
        uBeaconInterface = UBeacon.start(getApplicationContext(), builder.build());

    }

    private String getCurrentTime() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format((new Date())).toString();
    }

    AdsFragment mAdsFragment;

    public void setAdsFragment(AdsFragment f) {
        this.mAdsFragment = f;
    }

}
