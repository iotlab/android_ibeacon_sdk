package vn.com.vng.ibeaconsdk.demo;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import vn.com.vng.ibeaconsdk.R;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.stop) {
            ((MApplication) getApplicationContext()).stop();
        } else if (id == R.id.start) {
            ((MApplication) getApplicationContext()).start();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.start).setOnClickListener(this);
        findViewById(R.id.stop).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((MApplication) getApplicationContext()).setActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ((MApplication) getApplicationContext()).setActivity(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
