package vn.com.vng.ibeaconsdk.request;

import android.provider.SyncStateContract;

import org.json.JSONObject;

/**
 * Created by AnhHieu on 6/1/15.
 */
public abstract class AbstractRequestEx extends Request {
    protected final String TAG = this.getClass().getSimpleName();

    public AbstractRequestEx() {
        super(null, null);
    }

    @Override
    public String getUrl() {
        return Constants.URL_HOME;
    }

    @Override
    public boolean validateData(Object data) {

        return true;
    }

    @Override
    public boolean checkDataAvailablity() {
        return false;
    }

    @Override
    public Object parseResponse(String response) throws Exception {
        JSONObject json = new JSONObject(response);
        int errorCode = json.optInt("error", 1);
        if (errorCode == 0) {
            return parseData(json.optString("data"));
        } else {
            throw new Exception(json.optString("message"));
        }
    }

    public abstract Object parseData(String data) throws Exception;

    @Override
    public void saveData(Object data) {

    }
}
