package vn.com.vng.ibeaconsdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by AnhHieu on 6/8/15.
 */
public class AdsData implements Parcelable {
    long appId;
    long beaconId;
    String toGetAdsUrl;
    String lastestAds;
    String deviceIdentify;

    public AdsData() {
    }

    public AdsData(Parcel in) {
        appId = in.readLong();
        beaconId = in.readLong();
        toGetAdsUrl = in.readString();
        lastestAds = in.readString();
        deviceIdentify = in.readString();
    }

    public AdsData parse(JSONObject data) {
        appId = data.optLong("appId");
        beaconId = data.optLong("beaconId");
        toGetAdsUrl = data.optString("toGetAdsUrl");
        lastestAds = data.optString("lastestAds");
        deviceIdentify = data.optString("deviceIdentify");
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(appId);
        dest.writeLong(beaconId);
        dest.writeString(toGetAdsUrl);
        dest.writeString(lastestAds);
        dest.writeString(deviceIdentify);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public AdsData createFromParcel(Parcel in) {
            return new AdsData(in);
        }

        public AdsData[] newArray(int size) {
            return new AdsData[size];
        }
    };

    @Override
    public String toString() {
        return super.toString();
    }

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }

    public long getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(long beaconId) {
        this.beaconId = beaconId;
    }

    public String getToGetAdsUrl() {
        return toGetAdsUrl;
    }

    public void setToGetAdsUrl(String toGetAdsUrl) {
        this.toGetAdsUrl = toGetAdsUrl;
    }

    public String getLastestAds() {
        return lastestAds;
    }

    public void setLastestAds(String lastestAds) {
        this.lastestAds = lastestAds;
    }

    public String getDeviceIdentify() {
        return deviceIdentify;
    }

    public void setDeviceIdentify(String deviceIdentify) {
        this.deviceIdentify = deviceIdentify;
    }
}
