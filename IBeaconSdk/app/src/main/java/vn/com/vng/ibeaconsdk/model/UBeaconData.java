package vn.com.vng.ibeaconsdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by AnhHieu on 6/1/15.
 */
public class UBeaconData implements Parcelable {


    long beaconId;
    String uuid;
    int bMajor;
    int bMinor;
    double lat;
    double lng;
    boolean enable;
    String partnerIds;
    int numberPartner;
    String beaconName;
    String description;


    public UBeaconData(JSONObject data) {
        beaconId = data.optInt("beaconId");
        uuid = data.optString("uuid");
        bMajor = data.optInt("bMajor");
        bMinor = data.optInt("bMinor");
        lat = data.optDouble("lat");
        lng = data.optDouble("lng");
        enable = data.optBoolean("enable");
        partnerIds = data.optString("partnerIds");
        numberPartner = data.optInt("numberPartner");
        beaconName = data.optString("beaconName");
        description = data.optString("description");
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getbMajor() {
        return bMajor;
    }

    public void setbMajor(int bMajor) {
        this.bMajor = bMajor;
    }

    public int getbMinor() {
        return bMinor;
    }

    public void setbMinor(int bMinor) {
        this.bMinor = bMinor;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getPartnerIds() {
        return partnerIds;
    }

    public void setPartnerIds(String partnerIds) {
        this.partnerIds = partnerIds;
    }

    public int getNumberPartner() {
        return numberPartner;
    }

    public void setNumberPartner(int numberPartner) {
        this.numberPartner = numberPartner;
    }

    public String getBeaconName() {
        return beaconName;
    }

    public void setBeaconName(String beaconName) {
        this.beaconName = beaconName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(long beaconId) {
        this.beaconId = beaconId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UBeaconData) {
            return beaconId == ((UBeaconData) o).getBeaconId();
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(beaconId);
        dest.writeString(uuid);
        dest.writeInt(bMajor);
        dest.writeInt(bMinor);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeInt(enable ? 1 : 0);
        dest.writeString(partnerIds);
        dest.writeInt(numberPartner);
        dest.writeString(beaconName);
        dest.writeString(description);
    }

    public UBeaconData(Parcel in) {
        beaconId = in.readLong();
        uuid = in.readString();
        bMajor = in.readInt();
        bMinor = in.readInt();
        lat = in.readDouble();
        lng = in.readDouble();
        enable = in.readInt() != 0 ? true : false;
        partnerIds = in.readString();
        numberPartner = in.readInt();
        beaconName = in.readString();
        description = in.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public UBeaconData createFromParcel(Parcel in) {
            return new UBeaconData(in);
        }

        public UBeaconData[] newArray(int size) {
            return new UBeaconData[size];
        }
    };
}
