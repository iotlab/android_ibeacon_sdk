package vn.com.vng.ibeaconsdk.demo;

import android.app.IntentService;
import android.content.Intent;
import android.widget.EditText;

import java.util.List;

import vn.com.vng.ibeaconsdk.BeaconConfig;
import vn.com.vng.ibeaconsdk.IBeaconListener;
import vn.com.vng.ibeaconsdk.R;
import vn.com.vng.ibeaconsdk.UBeaconInterface;
import vn.com.vng.ibeaconsdk.UBeaconManager;
import vn.com.vng.ibeaconsdk.model.AdsData;
import vn.com.vng.ibeaconsdk.model.UBeaconData;
import vn.com.vng.ibeaconsdk.utils.DebugUtil;

/**
 * Created by AnhHieu on 6/8/15.
 */
public class DemoService extends IntentService{
    private final String TAG="DemoService";
    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        UBeaconManager.isMainThread();
        return START_STICKY;
    }

    public DemoService() {
        super("DemoService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        UBeaconManager.isMainThread();

//        BeaconConfig.Builder builder = new BeaconConfig.Builder()
//                .appId(9L)
//                .interval(60*1000)
//                .beaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")
//                .listener(new IBeaconListener() {
//                    @Override
//                    public void onAdsChanged(AdsData data) {
////                        if (activity != null) {
////                            EditText et = (EditText) activity.findViewById(R.id.et_log);
////                            et.append("beaconId | " + data.getBeaconId() + " : " + data.getLastestAds());
////                        }
//                    }
//
//                    @Override
//                    public void onNearBeacons(List<UBeaconData> data) {
////                        if (activity != null) {
////                            EditText et = (EditText) activity.findViewById(R.id.et_log);
////                            //  et.setText(data.toArray().toString());
////                        }
//                    }
//                });
//        UBeaconInterface uBeaconInterface = UBeaconManager.start(getApplicationContext(), builder.build());
        //uBeaconInterface.lock();
        // uBeaconInterface.unlock();
    }

}
