package vn.com.vng.ibeaconsdk;

/**
 * Created by AnhHieu on 6/8/15.
 */
public class BeaconConfig {

    private long appId;
    private String beaconLayout;
    private IBeaconListener listener;
    private long intervalTime;

    private BeaconConfig() {
        intervalTime = 5 * 60 * 1000;
        appId = -1;
    }

    public long getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(long intervalTime) {
        this.intervalTime = intervalTime;
    }

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }

    public String getBeaconLayout() {
        return beaconLayout;
    }

    public void setBeaconLayout(String beaconLayout) {
        this.beaconLayout = beaconLayout;
    }

    public IBeaconListener getListener() {
        return listener;
    }

    public void setListener(IBeaconListener listener) {
        this.listener = listener;
    }

    public static class Builder {

        private BeaconConfig config;

        public Builder() {
            config = new BeaconConfig();
        }

        public Builder appId(long appId) {
            config.appId = appId;
            return this;
        }

        public Builder beaconLayout(String beaconLayout) {
            config.beaconLayout = beaconLayout;
            return this;
        }

        public Builder listener(IBeaconListener listener) {
            config.listener = listener;
            return this;
        }

        public Builder interval(long time) {
            config.intervalTime = time;
            return this;
        }

        public BeaconConfig build() {
            return config;
        }

    }
}
